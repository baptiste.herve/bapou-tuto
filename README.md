# Les tutos de Bapou

Je vais essayer le mettre le minimum syndical pour partager du code avec vous.

## Step 1: Get started

Pour commencer !

**Suivre le consignes d'installation du wiki** : https://gitlab.com/baptiste.herve/bapou-tuto/-/wikis/installation
- installer au moin python et git

**Cloner le projet localement**
- ouvrez un terminal de commande dans le répertoire de votre projet (que j'appellerais par la suite `PJ_DIR`) et faites la commande suivante :
```shell
git clone https://gitlab.com/baptiste.herve/bapou-tuto.git
```
- Un répertoire `bapou-tuto` doit être apparu avec moi-même ici => `${PJ_DIR}/bapou-tuto/README.md`

**Préparer son environnement python**
```shell
# se placer où il faut
cp ${PJ_DIR}/bapou_tuto

# créer un environnement virtuel de python dédié pour ce projet
python -m venv .venv
# Activer son environnement
${PJ_DIR}\bapou_tuto\.venv\Scripts\Activate.ps1

# La commande python et pip que vous utiliserez a présent feront référence à 
# un executable propre a votre projet

# Installer les dépendances
pip install -r requirement.txt
```

**GO GO tester que tout marche bien !**
```shell
# Appliquer une fonction au mot "poile"
python -m tuto test poile

# Lance un serveur web local
python -m tuto run
```
