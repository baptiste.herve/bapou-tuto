import sys

from tuto.fonction import ma_fonction
from tuto.app import app


if __name__ == "__main__":
    print("VICTORY")
    args = sys.argv[1:]

    if len(args) >= 2 and args[0] == "test":
        print(f"Appliquation de ma fonction a '{args[1]}'")
        resultat = ma_fonction(args[1])
        print(resultat)

    elif len(args) >= 1 and args[0] == "run":
        print("Running server")
        app.run(host="127.0.0.1", port=5000)

    if len(args) == 0:
        print("Choose between 'run' or 'test <MOT>'")

