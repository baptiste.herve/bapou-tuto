import json
from flask import Flask, request, render_template, url_for

from tuto.fonction import ma_fonction


# Initialise l'application à déployer
app = Flask(__name__)
app.config.from_file("config.json", load=json.load)


# Créer la route "/"
@app.route("/", methods=["GET", "POST"])
def hello():
    # Si la requête est de type GET (demande de chargement d'une page d'un navigateur)
    if request.method == "GET":
        return render_template(
            "index.html",  # Référence de template dans tuto/templates
            title="Victory!",  # variable 'titre' à transmettre au template
            image=url_for('static', filename="laurine_licorne.png"),  # chemin pour l'image a charger
        )

    # Sinon, la requête est POST (interrogation d'un formulaire)
    elif request.method == "POST":
        # Récupère la valeur transmise dans le formulaire ("mot")
        mot = request.form.get("mot")
        return ma_fonction(mot)
